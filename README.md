# HEX to code

Ruby script that takes a 6 digit HEX value, and prints the Swift SpriteKit code for that HEX colour as an SKColour. 

I found myself so often converting a colour from a Photoshop or Illustrator mockup from a HEX value to code, that to save time I wrote this quick Ruby script, and added it to my bin so I could run it like a bash command.

## Usage
```
$ hctc AA3383
Swift Code:
SKColor(red:0.664, green:0.199, blue:0.512, alpha:1) /* #AA3383 */
```


## To set up bash command

- Make sure permissions are correct:
```
chmod a+x hctc.rb
```

- Then rename it and move it to your bin dir
```
mkdir -p ~/bin
mv hctc.rb ~/bin/hctc
```