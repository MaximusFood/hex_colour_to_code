#!/usr/bin/env ruby

# Add some error checking code.

hex = ARGV[0].upcase

if hex[0] == '#' then hex = hex[1..-1] end

colours = hex.scan(/[\dA-F]{2}/).map {|c| c.to_i(16).to_f/256 }

puts "Swift Code:"
print "SKColor(red:%.3f, green:%.3f, blue:%.3f, alpha:1) " % colours
puts "/*\##{hex}*/" 

#TODO: Add Objective-C as well